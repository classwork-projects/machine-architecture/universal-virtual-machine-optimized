#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "assert.h"
#include "um.h"
#include "stack.h"
#include "seq.h"

// globals
#define NUM_REGISTERS 8

static uint32_t zero = 0;
static uint64_t one = 1;

/* STRUCTS */
// universal machine struct
struct UM_T {
    uint32_t registers[NUM_REGISTERS];
    uint32_t counter;
    Stack_T  available_IDs;
    Seq_T    segments;
};

// segments struct
typedef struct Seg_T {
    bool            is_Mapped;
    unsigned        size;
    uint32_t        identifier;
    uint32_t        *words;
} *Seg_T;


/* BITPACK */
const unsigned max_width = 64;
static inline uint64_t shift_word(uint64_t word, unsigned offset, bool right)
{
    if (offset >= 64)
    {
        word = 0;
    }
    else if (right)
    {
        word = (word >> offset);
    }
    else { // left shift
        word = (word << offset);
    }

    return word;
}

uint64_t Bitpack_newu(uint64_t word, unsigned width, unsigned lsb, uint64_t value)
{
    int64_t temp = ~0;
    temp = shift_word(temp, (max_width - width), false);
    temp = shift_word(temp, (max_width - width - lsb), true);
    value = shift_word(value, lsb, false);
    value = (value & temp);
    word = (word & ~temp);
    word = (word | value);

    return word;
}

uint64_t Bitpack_getu(uint64_t word, unsigned width, unsigned lsb)
{
    uint64_t temp = ~0;
    temp = shift_word(temp, (max_width - width), false);
    temp = shift_word(temp, (max_width - lsb - width), true);
    word = (word & temp);
    word = shift_word(word, lsb, true);

    return word;
}


/* SEGMENTS */

// given a UM struct and segment ID, return a particular segment from the sequence
static inline Seg_T get_seg(UM_T UM, unsigned segmentID)
{
    return Seq_get(UM->segments, segmentID);
}

Seg_T new_Seg(int size, uint32_t identifier)
{
    Seg_T seg = malloc(sizeof(*seg));       //allocate memory for seg

    assert(seg != NULL);

    // initial properties
    seg->is_Mapped = true;
    seg->size = size;
    seg->identifier = identifier;
    seg->words = (uint32_t *)malloc(sizeof(uint32_t) * size);   //allocate space for words


    // Each word in the new seg is initialized to zero
    for (int i = 0; i < size; i++)
    {
        (seg->words)[i] = 0;
    }

    //everything initialized, return new seg
    return seg;
}


void free_Seg(Seg_T *seg)
{
    free(*seg);
}


uint32_t get_word(Seg_T seg, unsigned index)
{
    return  (seg->words)[index];

}


void insert_word(Seg_T seg, unsigned index, uint32_t word)
{
    (seg->words)[index] = word;
}


int get_Size(Seg_T seg)
{
    return seg->size;
}


uint32_t get_identifier(Seg_T seg)
{
    return seg->identifier;
}



uint32_t Seg_map(Seg_T seg, int size)
{
    if (seg->is_Mapped) return false;   // can't map an already mapped seg

    free(seg->words);


    // initial properties
    seg->is_Mapped = true;
    seg->size = size;
    seg->words = (uint32_t *)malloc(sizeof(uint32_t) * size);


    // Each word in the new seg is initialized to zero
    for (int i = 0; i < size; i++)
    {
        (seg->words)[i] = 0;
    }

    return true;
}

// unmap given seg
void Seg_unmap(Seg_T seg)
{
    seg->is_Mapped = false;
}

// return true if seg is already mapped
bool is_Mapped(Seg_T seg)
{
    return seg->is_Mapped;
}


/* UNIVERSAL MACHINE */

// gets a word from a given segment
static inline uint32_t get_seg_word(UM_T UM, uint32_t segmentID, unsigned offset)
{
    return get_word(get_seg(UM, segmentID), offset);
}

// sets a word value for the given segment
static inline void edit_seg_word(UM_T UM, uint32_t segmentID, unsigned offset, uint32_t word)
{
    insert_word(get_seg(UM, segmentID), offset, word);
}

// map a segment of given size - number of words
static inline uint32_t map_UM_seg(UM_T UM, unsigned num_words)
{
    // get ID from stack and map segment
    if (Stack_empty(UM->available_IDs) == 0)
    {
        uint32_t segmentID = (uint32_t)(uintptr_t)Stack_pop(UM->available_IDs);
        Seg_T seg = get_seg(UM, segmentID);
        Seg_map(seg, num_words);
        return segmentID;
    }
    // add new segment
    Seg_T new_seg = new_Seg(num_words, Seq_length(UM->segments));
    Seq_addhi(UM->segments, new_seg);

    return get_identifier(new_seg);
}

// unmaps given segment, rendering it unusable to the UM
static inline void unmap_UM_seg(UM_T UM, uint32_t segmentID)
{

    // unmap
    Seg_unmap(get_seg(UM, segmentID));
    // make segmentID available
    Stack_push(UM->available_IDs, (void *)(uintptr_t)segmentID);
}

/* gets the value of a specified register */
static inline uint32_t get_register_val(UM_T UM, unsigned reg)
{
    return (UM->registers)[reg];
}

// change the given registers value
static inline void change_register_word(UM_T UM, unsigned reg, uint32_t word)
{
    (UM->registers)[reg] = word;
}

/* INSTRUCTIONS */

// moves value between two registers
static inline void conditional_move(UM_T UM, int rA, int rB, int rC)
{
    if (get_register_val(UM, rC) != 0)
    {
        change_register_word(UM, rA, get_register_val(UM, rB));
    }
}

// loads a value from given segment, stores into the given register
static inline void segmented_load(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    change_register_word(UM, rA, get_seg_word(UM, rB_val, rC_val));
}

// loads a value from given register, stores into the given segment
static inline void segmented_store(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rA_val = get_register_val(UM, rA);
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    edit_seg_word(UM, rA_val, rB_val, rC_val);
}

// addition of values stored in two registers
static inline void addition(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    uint32_t sum = (rB_val + rC_val) % (one << 32);
    change_register_word(UM, rA, sum);
}

// multiplication of values stored in two register
static inline void multiplication(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    uint32_t product = (rB_val * rC_val) % (one << 32);
    change_register_word(UM, rA, product);
}

// division of values stored in two registers
static inline void division(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    uint32_t quotient = (rB_val / rC_val);
    change_register_word(UM, rA, quotient);
}

// returns a NAND on two values in two registers
static inline void bitwise_NAND(UM_T UM, int rA, int rB, int rC)
{
    uint32_t rB_val = get_register_val(UM, rB);
    uint32_t rC_val = get_register_val(UM, rC);
    uint32_t NAND = ~(rB_val & rC_val);
    change_register_word(UM, rA, NAND);
}

// signal end of program
static inline void halt(UM_T UM, int rA, int rB, int rC)
{
    // do nothing
    (void) UM;
    (void) rA;
    (void) rB;
    (void) rC;
}

// maps a new segment with given length and stores segmentID into a register
static inline void map_segment(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    change_register_word(UM, rB, map_UM_seg(UM, get_register_val(UM, rC)));
}

// unmaps a specified segment
static inline void unmap_segment(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    (void) rB;
    int segmentID = get_register_val(UM, rC);
    unmap_UM_seg(UM, segmentID);
}

// outputs a ASCII value from given register
static inline void output(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    (void) rB;
    uint32_t c = get_register_val(UM, rC);
    // unsigned c is always >= 0
    if (c <= 255)
    {
        putchar(c);
    }
}

// read in a character from input and store in a register
static inline void input(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    (void) rB;
    char c = getchar();
    if (c == EOF)
    {
        change_register_word(UM, rC, (~zero));
    }
    else {
        change_register_word(UM, rC, c);
    }
}

// load given memory segment into m[0]
static inline void load_program(UM_T UM, int rA, int rB, int rC)
{
    (void) rA;
    (void) rC;

    // if loading m[0], skip
    if (get_register_val(UM, rB) == 0)
    {
        return;
    }

    // get segment m[rB_val] length
    uint32_t segmentID = get_register_val(UM, rB);

    int seg_length = get_Size(get_seg(UM, segmentID));

    // unmap m[0] and remap with new length from m[rB_val]
    unmap_UM_seg(UM, 0);
    map_UM_seg(UM, seg_length);

    // copy contents of m[rB_val] into m[0]
    for (int i = 0; i < seg_length; i++)
    {
        uint32_t word = get_seg_word(UM, segmentID, i);
        edit_seg_word(UM, 0, i, word);
    }
}

// load a value and stores in given register
static inline void load_value(UM_T UM, int rA, int value)
{
    change_register_word(UM, rA, value);
}

// instructions indexable with the opcode
static void (*instructions[13]) (UM_T UM, int rA, int rB, int rC) = {
        conditional_move,
        segmented_load,
        segmented_store,
        addition,
        multiplication,
        division,
        bitwise_NAND,
        halt,
        map_segment,
        unmap_segment,
        output,
        input,
        load_program,
};


/* MAIN FUNCTIONS */

// initialize a new UM with no program instructions
UM_T new_UM()
{
    UM_T um = malloc(sizeof(*um));
    assert(um != NULL);

    um->counter = 0;
    um->available_IDs = Stack_new();

    for (int i = 0; i < NUM_REGISTERS; i++)
    {
        (um->registers)[i] = 0;
    }

    // initialize 0 segment m[0]
    um->segments = Seq_new(1000);
    Seq_addhi(um->segments, new_Seg(5, 0));

    return um;
}


// read in the file containing a UM program to run and store these instructions in m[0]
void read_UM(UM_T UM, FILE *fp)
{
    // read each instruction to new sequence
    Seq_T instructions = Seq_new(5);
    while (true)
    {
        uint32_t instruction = 0;
        // read and bitpack 4 characters from input line
        for (int i = 3; i >= 0; i--)
        {
            int c = fgetc(fp);
            if (c == EOF)
            {
                break;
            }
            instruction = Bitpack_newu(instruction, 8, i*8, c);
        }

        // stop reading if end of file
        if (feof(fp) != 0)
        {
            break;
        }
        // else add instruction to program instructions sequence
        Seq_addhi(instructions, (void *)(uintptr_t)instruction);
    }

    // remap segment 0 for instructions
    unmap_UM_seg(UM, 0);
    map_UM_seg(UM, Seq_length(instructions));

    // put each instruction from sequence in segment 0
    for (int i = 0; i < Seq_length(instructions); i++)
    {
        uint32_t instruction = (uint32_t)(uintptr_t)Seq_get(instructions, i);
        edit_seg_word(UM, 0, i, instruction);
    }

    // free instructions sequence
    Seq_free(&instructions);
}


// run the supplied program to completion
void run_UM(UM_T UM)
{
    while (true) {


        // get instruction and it's opcode
        uint32_t instruction = get_seg_word(UM, 0, UM->counter);
        int opcode = Bitpack_getu(instruction, 4, 28);

        if (opcode == 13)   // instruction: load_value
        {
            int regA  = Bitpack_getu(instruction, 3, 25);
            int value = Bitpack_getu(instruction, 25, 0);
            load_value(UM, regA, value);
        }
        else if (opcode == 7)   // instruction: halt
        {
            break;

        }
        else {  // other instructions
            int regA = Bitpack_getu(instruction, 3, 6);
            int regB = Bitpack_getu(instruction, 3, 3);
            int regC = Bitpack_getu(instruction, 3, 0);
            instructions[opcode](UM, regA, regB, regC);
        }

        // instruction: load_program (adjust counter)
        if (opcode == 12)
        {
            int regC = Bitpack_getu(instruction, 3, 0);
            UM->counter = (UM->registers)[regC];
        }
        else { // other instructions
            (UM->counter)++;
        }
    }
}


// free all UM memory
void free_UM(UM_T *UM_p)
{
    assert(UM_p && *UM_p);
    UM_T um = *UM_p;

    // free all segments
    for (int i = 0; i < Seq_length(um->segments); i++)
    {
        Seg_T seg = get_seg(um, i);
        free_Seg(&seg);
    }
    // free sequence and UM struct
    Seq_free(&(um->segments));
    free(um);
}
