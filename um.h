#ifndef UM_INCLUDED
#define UM_INCLUDED

#include <inttypes.h>

typedef struct UM_T *UM_T;

UM_T     new_UM();
void     read_UM(UM_T UM, FILE *fp);
void     run_UM(UM_T UM);
void     free_UM(UM_T *UM);

#endif