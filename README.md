# Universal Virtual Machine - Optimized

This project consisted of using code-tuning techniques and tools such as callgrind and kcachegrind to improve the performance of the universal virtual machine developed by myself and Marcus Ferraro.  

The unoptimized project can be found here: https://gitlab.com/classwork-projects/machine-architecture/universal-virtual-machine 


## Analysis and Improvements

**Source Code**  
The routine in the final UM that takes the most time is the run_UM
function. This function is responsible for the bulk of the activity, specifically in that it handles getting and calling instructions. Given a program, it will run it until completion. A significant amount of the runtime can be attributed to the use of bitpack for handling instructions. In general, the functions are as (reasonably) efficient as they can be. The run_UM function can be attributed most of the runtime
as we changed the majority of essential functions to be static inline. By inlining the other functions, we eliminate the need for the compiler to make them externally visible.

One major improvement we attempted was replacing the existing hanson
sequence with a dynamic c array. While this would have made a noticeable time reduction, we kept running into a segmentation fault because of what seemed to be an issue with building the array. When it tried to access it at index 0 there was a bad memory write. We attempted to
resolve this by properly allocating our struct and then allocating words, and having seg->words point to the proper address returned by the second malloc, but were unsuccessful. Due to time constraints, we reverted the related changes involved so we can still run our previous tests.

**Assembly**  
While there are some potential ways to improve the assembly code, none of them seem to be trivial. In our analysis of the assembly code, we observed that a lot of the code is related to Bitpack operators. Perhaps the number of lines could be reduced if we could use or create an alternative to bitpack to store our information. Aside from bitpack, the computations performed appear to be reasonably efficient, and seem to avoid unnecessary or redundant calls. Without drastic changes like a new bitpack, it is unlikely that the assembly code can be noticeably improved.

## Benchmarks
| Benchmark 	| Time 	| Instructions - midmark only [valgrind --tool=callgrind ./um /csc/411/um/midm ark.um] 	| Rel to start [The wall-clock time of this stage divided by the wall-clock time of the initial stage (time relative to start)] 	| Rel to prev [The wall-clock time of this stage divided by the wall-clock time of the previous stage (time relative to previous stage)] 	| Improvement 	|
|---------------------------------	|-----------------------	|--------------------------------------------------------------------------------------	|-------------------------------------------------------------------------------------------------------------------------------	|----------------------------------------------------------------------------------------------------------------------------------------	|---------------------------------------------------------------------------------------------------------	|
| Big (sandmark) small (midmark) 	| 3m30.557s<br /><br />8.418s 	| 49,018 *10^6 	| 1.0 	| 1.0 	| No improvement (starting point) 	|
| Big (sandmark) small (midmark) 	| 2m21.738s<br /><br />5.694s 	| 39,610*10^6 	| 0.66128881755 	| 0.66128881755 	| Compiled with optimization O1 turned on 	|
| Big (sandmark) small (midmark) 	| 2m3.552s<br /><br />4.978s 	| 37.214*^10^6 	| 0.57644072857 	| 0.87169284172 	| Compiled with optimization O2 turned on 	|
| Big (sandmark) small (midmark) 	| 1m45.228s<br /><br />4.208s 	| 24,362 * 10^6 	| 0.49976015995 	| 0.85168997669 	| Declare required bitpack functions in um.c with unnecessary validations removed. 	|
| Big (sandmark)  small (midmark) 	| 1m43.197s<br /><br />4.141s 	| 20,689 * 10^6 	| 0.49011431583 	| 0.98069905348 	| Modify implementation of segment function types to static inline in um.c 	|
| Big (sandmark) small (midmark) 	| 1m.37.844s<br /><br />3.921s 	| 18,468 * 10^6 	| 0.46469127124 	| 0.94810077519 	| Removed redundant/ unnecessary assertions and changed numerous additional functions to be static inline 	|
